<?php

namespace Drupal\idlc\Plugin\ConfigFilter;

use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a ignore filter for disabled languages.
 *
 * @ConfigFilter(
 *   id = "disabled_languages_config_ignore",
 *   label = "disabled languages config ignore",
 *   weight = 101
 * )
 */
class DisabledLanguagesConfigIgnore extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new ConfigFilter.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function filterDelete($name, $delete) {
    if (!$delete) {
      return FALSE;
    }
    $langcodes = $this->languageManager->getLanguages();
    $langcodesList = [];

    foreach (array_keys($langcodes) as $key => $value) {
      $langcodesList[$key] = 'language.' . $value;
    }

    if (empty($this->getSourceStorage()->getCollectionName()) || in_array($this->getSourceStorage()->getCollectionName(), $langcodesList)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function filterListAll($prefix, array $data) {
    $langcodes = $this->languageManager->getLanguages();
    $langcodesList = [];

    foreach (array_keys($langcodes) as $key => $value) {
      $langcodesList[$key] = 'language.' . $value;
    }

    if (empty($this->getSourceStorage()->getCollectionName()) || in_array($this->getSourceStorage()->getCollectionName(), $langcodesList)) {
      return $data;
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function filterDeleteAll($prefix, $delete) {
    $langcodes = $this->languageManager->getLanguages();
    $langcodesList = [];

    foreach (array_keys($langcodes) as $key => $value) {
      $langcodesList[$key] = 'language.' . $value;
    }

    if (empty($this->getSourceStorage()->getCollectionName()) || in_array($this->getSourceStorage()->getCollectionName(), $langcodesList)) {
      return $delete;
    }
    else {
      return [];
    }
  }

}
